const MarsRover = require('./MarsRover.js');

const firstRover = new MarsRover();
const secondRover = new MarsRover({x: 2, y: 4}, "WEST");


firstRover.moveForward();
firstRover.turnRight();
firstRover.moveForward();
firstRover.turnLeft();
firstRover.moveForward();
firstRover.printPosition();
firstRover.printLog();

secondRover.moveForward();
secondRover.turnRight();
secondRover.moveForward();
secondRover.turnLeft();
secondRover.moveForward();
secondRover.printPosition();
secondRover.printLog();
