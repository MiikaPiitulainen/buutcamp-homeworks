const userName = document.getElementById("user-name");
const userPost = document.getElementById("user-post");
const submitPost = document.getElementById("submit-post");
const allPosts = document.getElementById("forum-posts");


// You don't necessarily need this for anything,
// Just showing how addEventListener & "event" works
submitPost.addEventListener("click", () =>{

    let postedUsername = document.createElement("p");
    let postedUserPost = document.createElement("p");
    let hrTag = document.createElement("hr");

    postedUsername.innerHTML = userName.value;
    postedUserPost.innerHTML = userPost.value;
    allPosts.appendChild(postedUsername);
    
    allPosts.appendChild(postedUserPost);
    allPosts.appendChild(hrTag);
})
