class MarsRover {

    constructor(testPosition={x: 5, y: 0}, testDirection ="NORTH") {
        this.position = testPosition;
        this.direction = testDirection;
        this.log = [];
        this.log.push({ x: this.position.x, y: this.position.y });
    }

    printPosition() {
        console.log("currently positioned in " + this.position.x + "," + this.position.y);
    }
    printLog() {
        console.log("travel log:")
        for (let i = 0; i < this.log.length; i++) {
            console.log(this.log[i].x + "," +this.log[i].y);
        }
    }

    moveForward() {
        switch (this.direction) {
            case "EAST":
                this.position.x -= 1;
                break;
            case "WEST":
                this.position.x += 1;
                break;
            case "NORTH":
                this.position.y += 1;
                break;
            case "SOUTH":
                this.position.y -= 1;
                break;
        }
        this.log.push({ x: this.position.x, y: this.position.y });
        console.log("moving towards " + this.direction+ " to coordinates "+ this.position.x + "," +this.position.y);
    }

    turnRight() {
        switch (this.direction) {
            case "EAST":
                this.direction = "SOUTH";
                break;
            case "WEST":
                this.direction = "NORTH";
                break;
            case "NORTH":
                this.direction = "EAST";
                break;
            case "SOUTH":
                this.direction = "WEST";
                break;
        }
        console.log("Turning right to face "+this.direction);

    }
    turnLeft() {
        switch (this.direction) {
            case "EAST":
                this.direction = "NORTH";
                break;
            case "WEST":
                this.direction = "SOUTH";
                break;
            case "NORTH":
                this.direction = "WEST";
                break;
            case "SOUTH":
                this.direction = "EAST";
                break;
        }
        console.log("Turning left to face " + this.direction);
    }
    
    
}
module.exports = MarsRover;
