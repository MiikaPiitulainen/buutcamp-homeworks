import React from 'react';
import ReactDOM from 'react-dom';

import GameRegion from './GameRegion';

const App = () => {
  return (
    <div>
      <GameRegion />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));