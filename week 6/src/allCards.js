import Kolmio from './images/kolmio.png'
import Kuusikulmio from './images/kuusikulmio.png'
import Nelio from './images/nelio.png'
import Puhekupla from './images/puhekupla.png'
import Sydan from './images/sydan.png'
import Tahti from './images/tahti.png'
import Viisikulmio from './images/viisikulmio.png'
import Vinonelio from './images/vinonelio.png'

export const allCards =
        [
            { image: Kolmio,        pairId: 1,   isShown: false, id: 1,  isGuessed: false },
            { image: Kuusikulmio,   pairId: 2,   isShown: false, id: 2,  isGuessed: false },
            { image: Nelio,         pairId: 3,   isShown: false, id: 3,  isGuessed: false},
            { image: Puhekupla,     pairId: 4,   isShown: false, id: 4,  isGuessed: false },
            { image: Sydan,         pairId: 5,   isShown: false, id: 5,  isGuessed: false },
            { image: Tahti,         pairId: 6,   isShown: false, id: 6,  isGuessed: false},
            { image: Viisikulmio,   pairId: 7,   isShown: false, id: 7,  isGuessed: false},
            { image: Vinonelio,     pairId: 8,   isShown: false, id: 8,  isGuessed: false},
            { image: Kolmio,        pairId: 1,   isShown: false, id: 9,  isGuessed: false},
            { image: Kuusikulmio,   pairId: 2,   isShown: false, id: 10, isGuessed: false },
            { image: Nelio,         pairId: 3,   isShown: false, id: 11, isGuessed: false },
            { image: Puhekupla,     pairId: 4,   isShown: false, id: 12, isGuessed: false },
            { image: Sydan,         pairId: 5,   isShown: false, id: 13, isGuessed: false },
            { image: Tahti,         pairId: 6,   isShown: false, id: 14, isGuessed: false },
            { image: Viisikulmio,   pairId: 7,   isShown: false, id: 15, isGuessed: false},
            { image: Vinonelio,     pairId: 8,   isShown: false, id: 16, isGuessed: false }
        ]
        function shuffle(array) {
            return array.sort(() => Math.random() - 0.5);
        }
        shuffle(allCards);

export default allCards.map((card,ind) => { return {...card, id: ind} });