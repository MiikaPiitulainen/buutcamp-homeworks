import React, { useState } from 'react';

import Card from './Card.js';
import './gameRegion.css';
import  allCards  from './allCards.js'


const GameRegion = function (props) {
    let [gameState, setGamestate] = useState({
        allCards: allCards,
        freeze: false
    });

let openedCards = [];

if(gameState.freeze===false){
    openedCards = gameState.allCards.filter(card =>
        card.isShown === true &&
        card.isGuessed === false
    );
}

    if (openedCards.length === 2) {
        if (openedCards[0].pairId === openedCards[1].pairId) {

            const card1 = document.getElementById(`card_${openedCards[0].id}`)
            const card2 = document.getElementById(`card_${openedCards[1].id}`)
            card1.style.backgroundColor = "#00ef00";
            card2.style.backgroundColor = "#00ef00";
            
            const newCards = [...gameState.allCards];
            newCards[openedCards[0].id].isGuessed = true;
            newCards[openedCards[1].id].isGuessed = true;

            const newState = {
                allCards: newCards,
                freeze: gameState.freeze
            }
            setGamestate(newState)
            console.log("pari");
        }
        else {
            const card1 = document.getElementById(`card_${openedCards[0].id}`)
            const card2 = document.getElementById(`card_${openedCards[1].id}`)
            card1.style.backgroundColor = "#ff0000";
            card2.style.backgroundColor = "#ff0000";
            setTimeout(() => {
                const newCards = [...gameState.allCards];
                newCards[openedCards[0].id].isShown = false;
                newCards[openedCards[1].id].isShown = false;
                const newState = {
                    allCards: newCards,
                    freeze: false
                }
                setGamestate(newState)
                const card1 = document.getElementById(`card_${openedCards[0].id}`)
                const card2 = document.getElementById(`card_${openedCards[1].id}`)
                card1.style.backgroundColor = "#ffffff";
                card2.style.backgroundColor = "#ffffff";
            }, 1500);
            const newState={
                allCards: [...gameState.allCards],
                freeze: true
            }
            setGamestate(newState)
            console.log("ei paria")
        }
    }


    return (
        <div>
            <div id="game-region">
                {gameState.allCards.map((card, ind) => {

                    return (
                        <Card gameState={gameState} id={card.id} setCards={setGamestate}  key={ind} />
                    );
                })}
            </div>
        </div>
    );
}

export default GameRegion;
