//Homework 2.1

const numberOne= 5;
const numberTwo= 6;
const numberThree= -2;

function BigSmall(NumberOne, NumberTwo, NumberThree){
    console.log("the biggest number is:")
    console.log(Math.max(NumberOne, NumberTwo, NumberThree));
    console.log("the smallest number is:")
    console.log(Math.min(NumberOne, NumberTwo, NumberThree));
}

BigSmall(numberOne, numberTwo, numberThree);

//Homework 2.2

let nonVariable;

function CheckVariable(NonVariable){
    if(typeof NonVariable !== "undefined"){
        console.log("The variable has been declared")
    }
    else{
        console.log("The Variable has not been declared");
    }
}

CheckVariable(nonVariable);

//Homework 2.3

const positiveNumber=49;
const negativeNumber=-49;

function SquareRoot(SquareNumber){
    if(SquareNumber<0){
        console.log("the number is negative! Cannot calculate")
    }
    else{
        console.log("The square root of the given number is: "+ Math.sqrt(SquareNumber))
    }
}
SquareRoot(negativeNumber);
SquareRoot(positiveNumber);

//Homework 2.4

const variableX = 20;
const variableY = 1;
const variableZ = 1;

const variableA = -3;
const variableB = 4;
const variableC = 5;

function Quadric (X, Y, Z){

    if(Math.pow(Y, 2) - (4 * X * Z)<0){
        console.log("b^2-4ac result is negative! Stop the program!")
        return undefined;
    }
    else{

    var result = (-1 * Y + Math.sqrt(Math.pow(Y, 2) - (4 * X * Z))) / (2 * X);
    console.log("The quadric is: ");
    return result;
    }
}

console.log(Quadric(variableX, variableY, variableZ))
console.log(Quadric(variableA, variableB, variableC))