**Deadline: 02.06.2020**

Please save your home assignment as `H2_Firstname_Lastname.zip` and send it to the course teacher on Discord.

For all the exercises below, you can either wrap the functionality to a function and give the wanted input as function arguments *or* use command line arguments (`process.argv`).

### H2.1

Create a program that takes in three numbers, `number_1`, `number_2` and `number_3`. Decide their values freely. Find the 

a) largest one 
    
b) smallest one 

and `console.log()` its name and value.


### H2.2
Write a program that tests if a variable has been declared. Print the result.

### H2.3
Write a program that calculates a square root of `number`, and...

 a) if `number` is negative, prints out an error message.
    
 b) (Extra) if `number` is negative, calculates the complex square root.

### H2.4
Create a program that returns all the solutions for the quadratic equation $`ax^2 + bx + c = 0`$.

Give the parameters a, b and c as arguments for the program and return the roots that you get from the formula. 

$`x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}`$.

Meaning you can for example define a function in your program

```
const solveQuadratic = function(a,b,c) {
    // create algorithm here and for example console log the results for "x"
}
//and then call the function like, e.g.,
solveQuadratic(2,3,5)
```

You can use the function Math.sqrt() to calculate square root. E.g., Math.sqrt(123) calculates the square root of 123.

Remember to take into account that $`b^2 - 4ac`$ can have a negative value! **Note** that you can just print some kind of error message if the stuff inside the square root is negative.

### H2.5 (**Extra, might be a bit difficult**)
Create a program that prints the following looking pyramid.
Make it easy to change so that the user can just define the pyramid height
(as in the following case is 4) for the script to work.

So by calling a function createPyramid(4) the following structure would be printed

```
   &
  &&&
 &&&&&
&&&&&&&
```

and by calling createPyramid(5)

```
    &
   &&&
  &&&&&
 &&&&&&&
&&&&&&&&&
```







