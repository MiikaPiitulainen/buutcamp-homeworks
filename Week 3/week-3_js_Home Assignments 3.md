# Buutti bank command line interface

**Deadline: 10.6.2020 13:00**

Please save your home assignment as H3_Firstname_Lastname.zip and send it to the course teacher on discord.

### User input

A crucial part of this exercise is *user input*. For that, you need to install an npm package `readline-sync`.
    
    npm init
    npm install --save readline-sync

Then, try using it:

    const readline = require("readline-sync");
    let answer = readline.question("Give me an answer: ");
    console.log(answer);


## Basics

### H3.1 Welcome to the banking system

This is a really common exercise that many programmers do at the beginning of their career. The reason it's so common is because it teaches to use many different types of variables and operations. Remember the DRY rule - Don't repeat yourself! If you find yourself copy-pasting code, you should instead wrap it inside a function.

Even inside the terminal, UI is crucial part of an application. Below you’ll see what your application should say when you run it the first time.

    $ Welcome to Buutti banking CLI!
    $ Hint: You can get help with the commands by typing "help".

The application should accept 
After printing the text above, wait for user input. Input consists of multiple options, so instead of overwhelming ourselves, let's define the behavior for the `help` command first.

### H3.2 Help and quit

If the user enters the `help` command, the text below should be printed out (without the dollar signs). 

    $ Here’s a list of commands you can use!
    $ 
    $ help                Opens this dialog.
    $ quit                Quits the program.
    $ 
    $ Account actions
    $ create_account      Opens a dialog for creating an account.
    $ close_account       Opens a dialog for closing an account.
    $ change_name         Opens a dialog for changing the name associated with an account.
    $ does_account_exist  Opens a dialog for checking if an account exists.
    $ account_balance     Opens a dialog for logging in and prints the account balance.
    $ 
    $ Fund actions
    $ withdraw_funds      Opens a dialog for withdrawing funds.
    $ deposit_funds       Opens a dialog for depositing funds.
    $ transfer_funds      Opens a dialog for transferring funds to another account.
    $ 
    $ Fund requests
    $ request_funds       Opens a dialog for requesting another user for funds.
    $ fund_requests       Shows all the fund requests for the given account.
    $ accept_fund_request Opens a dialog for accepting a fund request.

The rest of the exercise consists of implementing all the features listed here. After running each command, the program should return to the beginning, where the user can input more commands.

Before jumping to more complex command chains, implement the `quit` command to exit the program.

## Accounts
### H3.3 Create an account 

The `create_account` command starts a dialog sequence where the user is asked multiple questions. You should store the user's answers in their respective variables---implementation is up to you. Below is an example dialog. (Lines starting with `$` are printed by the program, `>` denotes user input.)

    $ Creating a new user account!
    $ Insert your name.
    > Rene Orosz
    $ Hello, Rene Orosz! It’s great to have you as a client.
    $ How much is your initial deposit? (The minimum is 10€)
    > 8
    $ Unfortunately we can’t open an account for such a small account. Do you have any more cash with you?
    > 12
    $ Great, Rene Orosz! You now have an account with a balance of 12€.
    $ We’re happy to have you as a customer, and we want to ensure that your money is safe with us. 
    $ Give us a password, which gives only you the access to your account.
    > hunter12
    $ Your account is now created.
    $ Account id: 2035
    $ Store your account ID in a safe place.

The data you collect from the user should be stored into an object akin to this one:

    const account = {
        name: "Rene Orosz",
        password: "hunter12",
        id: 2035,
        balance: 12,
        fund_requests: []
    }

Generate the ID within the program---it should be unique for all the users (and additionally, preferably not just integers 1, 2, 3, ... for consecutive new users). User data objects should be stored in an array `all_users`.

### H3.4 Does an account exist

The `does_account_exist` command starts a dialog sequence as well. Find if the account in question is stored in `all_users` array.

    $ Checking if an account exists!
    $ Enter the account ID whose existence you want to verify.
    > 69420
    $ An account with that ID does not exist. Try again.
    > 2035
    $ This account exists.

### H3.5 Account balance

The `account_balance` command starts a dialog sequence as well. User is asked for an account ID and a password. Given a correct ID-password pair, the account balance is printed.

    $ Checking your account balance!
    $ What is your account ID?
    > 69420
    $ An account with that ID does not exist. Try again.
    > 2035
    $ Account found! Insert your password.
    > hunetr12
    $ Wrong password, try typing it again.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ Your account balance is 12€.

### H3.6 Change name

The `change_name` command starts a dialog sequence as well. You get the drill.

    $ Changing the name associated with your account!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ But it appears you want to change your name.
    $ Which name should we change your name to?
    > Rene Orozzz
    $ We will address you as Rene Orozzz from now on.

## Funds
### H3.7 Withdraw funds

The `withdraw_funds` command starts a dialog sequence as well. Reuse elements from H2.5.4 and H2.5.6 for checking if an account exists and logging in.

    $ Withdrawing cash!
    $ What is your account ID?
    > 69420
    $ An account with that ID does not exist. Try again.
    > 2035
    $ Account found! Insert your password.
    > hunetr12
    $ Wrong password, try typing it again.
    > hunter12.
    $ Correct password. We validated you as Rene Orozzz.
    $ How much money do you want to withdraw? (Current balance: 12€)
    > 13
    $ Unfortunately you don’t have the balance for that. Try a smaller amount.
    > 11
    $ Withdrawing a cash sum of 11€. Your account balance is now 1€.

Find the correct account from the `all_users` array, validate the user and update the account balance. Don't allow the user to exceed their balance in the withdrawal.

**Try to get at least this far - Rest is bonus**
---

### H3.8 Deposit funds

The `deposit_funds` command starts a dialog sequence as well, very similarly to `withdraw_funds`.

    $ Depositing cash!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orozzz.
    $ How much money do you want to deposit? (Current balance: 12€)    
    > 250
    $ Depositing 250€. Your account balance is now 262€.

### H3.9 Transfer funds

The `transfer_funds` command starts a dialog sequence as well.

    $ Transferring funds!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ How much money do you want to transfer? (Current balance: 262€)
    > 200
    $ Which account ID do you want to transfer these funds to?
    > 666
    $ An account with that ID does not exist. Try again.
    > 90570
    > Sending 11€ from account ID 2035 to account ID 90570.

## Requests
### H3.10 Request funds

The `request_funds` command starts a dialog sequence as well.

    $ Requesting funds!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ Which account ID do you request funds from?
    > 69420
    $ An account with that ID does not exist. Try again.
    > 90570
    $ Account found. How much money do you want to request?
    > 500
    $ Requesting 500€ from the user with ID 90570.

All the fund requests should be stored in a `fund_requests` array. It should contain the IDs of the requestee and the requester, and the requested amount.

### H3.11 Fund requests

The `request_funds` command lists the fund requests for your account.

    $ Listing fund requests!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ Listing all the requests for your account!
    $  - 420€ for the user 69420.
    $  - 69€ for the user 69420.
    $  - 2.60€ for the user 90570.

### H3.12 Accept fund request

The `accept_fund_request` command is somewhat self-explanatory and expands the previous command.

    $ Accepting fund requests!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ Listing all the requests for your account!
    $ 1.   420€ for the user 69420.
    $ 2.    69€ for the user 69420.
    $ 3.     2€ for the user 90570.
    $ Your account balance is 262€. Which fund request would you like to accept?
    > 1
    $ You do not have funds to accept this request.
    > 3
    $ Accepting fund request 2€ for the user 90570. 
    $ Transferring 2€ to account ID 90570.
    $ Your account balance is now 260€.

Remember to update the `fund_requests` array and balance of the both accounts.


## Extra

### H3.13 Yes or no
Implement a yes/no question, and add it as a confirmation to important actions, e.g. for accepting the fund request.

    $ Are you sure?
    > no
    $ Terminating current action.

    $ Are you sure?
    > yes
    
    
### H3.14 Log in

Implement a `log_in` command that asks for a username and a password. Store the logged in user ID in a `logged_user` variable. After logging in, other commands should validate the user automatically, and the log_in command is unavailable; instead, there should be a `log_out` command available.

    $ Logging in!
    $ What is your account ID?
    > 69420
    $ An account with that ID does not exist. Try again.
    > 2035
    $ Account found! Insert your password.
    > hunetr12
    $ Wrong password, try typing it again.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ You are now logged in.
    
### H3.15 Store the data in JSON

Store the user data in a JSON file which gets loaded when the program starts.